function [endo, epi] = getPointsFromCineData(dataDir, resPlane, resZ)
%
% This function extracts the point cloud from data coming from the CINE
% tool. The epicardium and endocardium are assumed to be marked in blue and
% red colors, respectively
%
% dataDir       -   the data directory
% resPlane      -   the in-plane resolution
% resZ          -   the out of plane resolution
%

% Some constants
epiColor = [0 0 255]; endoColor = [255 0 0];

% Range of slices
rangeSlices = [1 length(dir([dataDir '*.tif']))];

% Initialize the epi and endo arrays
epi = []; endo = []; zPos = 0;

% Read images one by one and do the extraction
for i = rangeSlices(1):rangeSlices(2)
    % Construct the file name
    suffix = num2str(i);
    if (length(suffix) == 1)
        suffix = ['00' suffix];
    elseif (length(suffix) == 2)
        suffix = ['0' suffix];
    end
    fileName = [dataDir suffix '.tif'];
    
    % Load the image
    slice = imread(fileName);
    
    % Extract endocardium
    sliceSelect = (slice(:, :, 1) == endoColor(1)).*(slice(:, :, 2) == endoColor(2)).*(slice(:, :, 3) == endoColor(3));
    [y, x] = find(sliceSelect);
    newPoints = [x*resPlane y*resPlane ones(length(x), 1)*zPos];
    endo = [endo; {newPoints}];
    
    % Extract epicardium
    sliceSelect = (slice(:, :, 1) == epiColor(1)).*(slice(:, :, 2) == epiColor(2)).*(slice(:, :, 3) == epiColor(3));
    [y, x] = find(sliceSelect);
    newPoints = [x*resPlane y*resPlane ones(length(x), 1)*zPos];
    epi = [epi; {newPoints}];
    
    % Increment zPos
    zPos = zPos + resZ;
end

%exist
%bwperim
%regexp
%hold 
%imread the whole file path without the 'r' as read only. 