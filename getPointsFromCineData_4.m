function [endo, epi] = getPointsFromCineData_4(dataDir, resPlane, resZ)
%                 Ivan Gramatikov, August 2012
%
%clear all;   % clears the workspace (once it's a function that takes parameters, comment out this line!!!)
 close all;   % closes all figures opened from a previous run.
         
    %***Temporary Placeholders
    %dataDir = '/home/ivan/Research/Install15/ischemic SLABS to fijoy/I group 0/JHU006SLABDEcontours/EndoEpi170x180/';
    %resZ = 1;  -   the out of plane resolution
    %resPlane = 1;   -   the in-plane resolution 


        

% epi= blue                endo= red
epiColor = [0 0 255]; endoColor = [255 0 0];

% Range of slices in the stack using wildcard for tifs.
rangeSlices = [1 length(dir([dataDir '*.tif']))];

% Initialize the epi and endo arrays
epi = []; endo = []; zPos = 0;

% Read images one by one and do the extraction
for i = rangeSlices(1):rangeSlices(2)
    % Construct the file name
    suffix = num2str(i);
    if (length(suffix) == 1)
        suffix = ['00' suffix];
    elseif (length(suffix) == 2)
        suffix = ['0' suffix];
    end
    fileName = [dataDir suffix '.tif'];
    
    % Load the image
    slice = imread(fileName);
    figure; imshow(slice);   % for debugging only
    fprintf ('\nData was read from %s\n',fileName);
    % Extract endocardium
    sliceSelect = (slice(:, :, 1) == endoColor(1)).*(slice(:, :, 2) == endoColor(2)).*(slice(:, :, 3) == endoColor(3));
    [y, x] = find(sliceSelect);
    newPoints = [x*resPlane y*resPlane ones(length(x), 1)*zPos];
    endo = [endo; {newPoints}];
    
    % Extract epicardium
    sliceSelect = (slice(:, :, 1) == epiColor(1)).*(slice(:, :, 2) == epiColor(2)).*(slice(:, :, 3) == epiColor(3));
    [y, x] = find(sliceSelect);
    newPoints = [x*resPlane y*resPlane ones(length(x), 1)*zPos];
    epi = [epi; {newPoints}];
    
    % change the z position or slice layer **
    zPos = zPos + resZ;
end

endo_level = 1;  % endo_level = 1 ... 9   if size(endo) = (9,1)
X1 = endo{endo_level,1};
X1b = X1(:,1);            % first column (X-coordinates), I might need to flip these later, not sure if rows or columns come first
Y1 = endo{endo_level,1};
Y1b = X1(:,2);            % second column (Y-coordinates)

mysize = size(X1b);
ScatterSize = mysize(1);
fprintf('\nNumber of points in endo = %i\n', ScatterSize);

% Display the selected layer
figure;
hold on;
for jj = 1:ScatterSize
  plot(X1b(jj),Y1b(jj),'r.'); 
  % fprintf('%i    %i , %i \n', jj, X1b(jj),Y1b(jj) );
end

SavedCurrentDirectory = pwd;   % store the working directory so the code can run.
cd (dataDir);  % go to the data directory 
cd('..');   % or simply  cd ..

if ( exist ('LVEndoMask','dir') == 7 )
    fprintf('\n LVEndoMask exists!\n');
    cd ('LVEndoMask');
    fprintf ('\nChanging directory to LVEndoMask\n');  % debug only
        %(WILL BE A LOOP over the number of files!) Here only for file #1
        % loop will run from  rangeSlices(1)  to  rangeSlices(2) as above
        % reading data from the LVEndoMask files
        
        % BW1 = imread('001.tif');  % reading the mask file 
        % figure; imshow(BW1);  % debug only
        % BW2 = bwperim(BW1,8); 
        % figure; imshow(BW2);  % debug only 
     
      
      %save original endo
      OriginalEndo = endo;
      % clear endo cell
      clear endo;
      
      % Read LVEndo files into endo   
      for i = rangeSlices(1):rangeSlices(2)
           % Construct the file name
            suffix = num2str(i);
           if (length(suffix) == 1)
                suffix = ['00' suffix];
              elseif (length(suffix) == 2)
                suffix = ['0' suffix];
           end
           % fileName = [dataDir suffix '.tif'];
           fileName = [suffix '.tif'];  % works in the current directory LVEndoMask
                                        % no full path name needed
    
          % Load the image
          % slice_mask = imread(fileName);
          % figure; imshow(slice_mask);   % for debugging only
             
          BW1 = imread(fileName);  % reading the mask file 
          fprintf ('\nData was read from %s\n',fileName);
          figure; imshow(BW1);  % debug only
          BW2 = bwperim(BW1,8); 
          figure; imshow(BW2);  % debug only
    
          %CurrentLayerSize = size (endo{1,1});  % wrong (endo not known)
          
          [Perim_row,Perim_col] = find(BW2);
          CurrentLayerSize = size(Perim_row);
          for kk = 1:CurrentLayerSize(1)
               endo{i,1}(kk,1) = Perim_row(kk);
               endo{i,1}(kk,2) = Perim_col(kk);
               endo{i,1}(kk,3) = i-1;
              
          end    
    
      end   % end of code when LVEndoMask exists
   
else   
    fprintf('\n LVEndoMask DOES NOT exist!\n');
    close all;
end

cd (SavedCurrentDirectory);  % restore the work directory
fprintf('\n\n Work directory restored to %s !\n',SavedCurrentDirectory);
fprintf('\nEND of Program \n\n');
close all


%exist
%bwperim
%regexp
%hold 
%imread the whole file path without the 'r' as read only. 
%find