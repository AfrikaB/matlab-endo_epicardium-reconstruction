% Test stuff for Ivan    filename:  Test_1_July_26_2012.m
% the script below is a short exercise in using cell arrays (containing
% dissimilar kinds of data) and reading the folders in a given directory

MaxNumberOfDirectories = 100;
NumberOfParametersPerDirectory = 5;

% Create a cell array
IvansCellArray = cell(MaxNumberOfDirectories,NumberOfParametersPerDirectory);
% Alternative:
% IvansCellArray{MaxNumberOfDirectories,NumberOfParametersPerDirectory} = [];

% Get all directory names contained in my main folder
% (each name can have a different size)
CurrentList = cell(System.IO.Directory.GetDirectories('C:\Boris\Matlab\ML_BG'));
% you can use any toot directory to start from
% this is a one-dimensional array

% get the number of all directories:
 [siz1,NumbFolders] = size(CurrentList); % we need the second one (NumbFolders) 
 
 % Print all directory names and load them into the Cell Array
 fprintf('\n\nList of all directories in the selected root directory:\n');
   for i = 1:NumbFolders
          IvansCellArray{i,1} = CurrentList{i};  % you can load the cell array cell-by-cell
                                                 % without a loop
          fprintf(' %3i  %s\n', i,IvansCellArray{i,1});
          % you can put some operation here, i.e. checking if each name
          % matches some string, like a folder name...
   end
   
   % at this moment, your directory names are loaded in the cell array