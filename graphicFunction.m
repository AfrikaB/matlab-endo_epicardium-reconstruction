function graphicFunction(x)
  
   y = 1.5*cos(x) + 6*exp(-.1*x) + exp(.07*x).*sin(3*x);

 
   ym = mean(y);

   hfig = figure('Name','Y versus X');
   hax = axes('Parent',hfig);
   plot(hax,x,y)


   
   % hold on puts pen down
   hold on
   plot(hax,[min(x) max(x)],[ym ym],'Color','red')
   hold off
   %hold off takes pen up and discontinues plotting from saved location.
   
   ylab = get(hax,'YTick');
   set(hax,'YTick',sort([ylab ym]))
   title ('y = 1.5cos(x) + 6e^{-0.1x} + e^{0.07x}sin(3x)')
   xlabel('X Axis'); ylabel('Y Axis')
end