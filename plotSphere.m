function plotSphere
k = 3;
n = 2^k-1;
[x,y,z] = sphere(n);
c = hadamard(2^k);
surf(x,y,z,c);
colormap([1  1  1; 0 0 0])
axis equal

end

