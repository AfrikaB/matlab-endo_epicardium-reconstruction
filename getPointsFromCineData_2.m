% function [endo, epi] = getPointsFromCineData(dataDir, resPlane, resZ)
%                 Ivan Gramatikov, August 2012
% This function extracts the point cloud from data coming from the CINE
% tool. The epicardium and endocardium are assumed to be marked in blue and
% red colors, respectively
%
clear all;   % clears the workspace (Debug only)
close all;   % close all figures

% debug only:  set the parameters that otherwise are passed to the function
dataDir = 'c:\home\ivan\Research\Install15\ischemic SLABS to fijoy\I group 0\JHU033contoursDEslab\EndoEpi150x180\';
resZ = 1;
resPlane = 2;


% dataDir       -   the data directory
% resPlane      -   the in-plane resolution
% resZ          -   the out of plane resolution
%

% Some constants
epiColor = [0 0 255]; endoColor = [255 0 0];

% Range of slices
rangeSlices = [1 length(dir([dataDir '*.tif']))];

% Initialize the epi and endo arrays
epi = []; endo = []; zPos = 0;

% Read images one by one and do the extraction
for i = rangeSlices(1):rangeSlices(2)
    % Construct the file name
    suffix = num2str(i);
    if (length(suffix) == 1)
        suffix = ['00' suffix];
    elseif (length(suffix) == 2)
        suffix = ['0' suffix];
    end
    fileName = [dataDir suffix '.tif'];
    
    % Load the image
    slice = imread(fileName);
    figure; imshow(slice);   % for debugging only
    fprintf ('\nData was read from %s\n',fileName);
    % Extract endocardium
    sliceSelect = (slice(:, :, 1) == endoColor(1)).*(slice(:, :, 2) == endoColor(2)).*(slice(:, :, 3) == endoColor(3));
    [y, x] = find(sliceSelect);
    newPoints = [x*resPlane y*resPlane ones(length(x), 1)*zPos];
    endo = [endo; {newPoints}];
    
    % Extract epicardium
    sliceSelect = (slice(:, :, 1) == epiColor(1)).*(slice(:, :, 2) == epiColor(2)).*(slice(:, :, 3) == epiColor(3));
    [y, x] = find(sliceSelect);
    newPoints = [x*resPlane y*resPlane ones(length(x), 1)*zPos];
    epi = [epi; {newPoints}];
    
    % Increment zPos
    zPos = zPos + resZ;
end

endo_level = 1;  % endo_level = 1 ... 9   if size(endo) = (9,1)
X1 = endo{endo_level,1};
X1b = X1(:,1);            % first column (X-coordinates)
Y1 = endo{endo_level,1};
Y1b = X1(:,2);            % second column (Y-coordinates)

mysize = size(X1b);
ScatterSize = mysize(1);
fprintf('\nNumber of points in endo = %i\n', ScatterSize);

% Display the layer
figure;
hold on;
for jj = 1:ScatterSize
  plot(X1b(jj),Y1b(jj),'o'); 
  % fprintf('%i    %i , %i \n', jj, X1b(jj),Y1b(jj) );
end

SavedCurrentDirectory = pwd;   % store temporarily the working directory
cd (dataDir);  % go to the data directory 
cd('..');   % or simply  cd ..

if ( exist ('LVEndoMask','dir') == 7 )
    fprintf('\n LVEndoMask exists!\n');
    cd ('LVEndoMask');
    fprintf ('\nChanging directory to LVEndoMask\n');  % debug only
        %(WILL BE A LOOP over the number of files!) Here only for file #1
        % loop will run from  rangeSlices(1)  to  rangeSlices(2) as above
        BW1 = imread('001.tif');  % reading the mask file 
        figure; imshow(BW1);  % debug only
        BW2 = bwperim(BW1,8); 
        figure; imshow(BW2);  % debug only
        % NEED TO ADD THE ACTUAL CODE HERE
else   
    fprintf('\n LVEndoMask DOES NOT exist!\n');
end

cd (SavedCurrentDirectory);  % restore the work directory
fprintf('\nEND of Program \n\n');


%exist
%bwperim
%regexp
%hold 
%imread the whole file path without the 'r' as read only. 